﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScroll : MonoBehaviour {
    public float fadePos = 300f;
    public float spawnPos = 250f;
    //public float speed = 30f;
    Vector3 dir;




    private void Start()
    {
        if (WorldVars.alive == true)
        {
            dir = Vector3.back;
        }
        else if (WorldVars.alive == false)
        {
            dir = Vector3.forward;
        }
    }
    void Update () {

        if (WorldVars.alive == true)
        {
            dir = Vector3.back;
        }
        else if (WorldVars.alive == false)
        {
            dir = Vector3.forward;
        }

        if (transform.position.z >= fadePos || transform.position.z <= fadePos * -1)
        {
            if (WorldVars.alive == true)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, spawnPos);
            }
            else if (WorldVars.alive == false)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, spawnPos * -1);
            }
            


        } else
        {
            transform.position += dir * WorldVars.worldSpeed * Time.deltaTime;
        }
        


    }
}
