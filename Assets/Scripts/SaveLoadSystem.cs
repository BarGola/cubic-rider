﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadSystem : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public static void StartSetup()
    {
        CheckValues();
        WorldVars.money = GetMoney();
        GetStats();
    }

    public static void CheckValues()
    {
        if(!PlayerPrefs.HasKey("Money"))
        {
            PlayerPrefs.SetInt("Money", 0);
        }
        if (!PlayerPrefs.HasKey("Highscore"))
        {
            PlayerPrefs.SetInt("Highscore", 0);
        }
        if (!PlayerPrefs.HasKey("DMG"))
        {
            PlayerPrefs.SetInt("DMG", 1);
        }
        if (!PlayerPrefs.HasKey("FR"))
        {
            PlayerPrefs.SetFloat("FR", 100);
        }
    }



    public static void SaveMoney()
    {
        PlayerPrefs.SetInt("Money", WorldVars.money);
    }


    public static int GetMoney()
    {
        if(PlayerPrefs.HasKey("Money"))
        {
            return PlayerPrefs.GetInt("Money");
        }
        else
        {
            return 0;
        }
        
    }


    public static void SaveHighscore(int a)
    {
        if(PlayerPrefs.HasKey("Highscore"))
        {
            int temp = PlayerPrefs.GetInt("Highscore");
            if(temp < a)
            {
                PlayerPrefs.SetInt("Highscore", a);
            }
        }
        else
        {
            PlayerPrefs.SetInt("Highscore", a);
        }
        
    }


    public static int GetHighscore()
    {
        if (PlayerPrefs.HasKey("Highscore"))
        {
            return PlayerPrefs.GetInt("Highscore");
        }
        else
        {
            return 0;
        }
    }

    public static void SaveStats()
    {
        PlayerPrefs.SetInt("DMG", Stats.damage);
        PlayerPrefs.SetFloat("FR", Stats.fireRate);

    }

    public static void GetStats()
    {
        Stats.damage = GetDMG();
        Stats.fireRate = GetFR();
    }

    public static int GetDMG()
    {
        if (PlayerPrefs.HasKey("DMG"))
        {
            return PlayerPrefs.GetInt("DMG");
        }
        else
        {
            return 1;
        }
    }

    public static float GetFR()
    {
        if (PlayerPrefs.HasKey("FR"))
        {
            return PlayerPrefs.GetFloat("FR");
        }
        else
        {
            return 1;
        }
    }

}
