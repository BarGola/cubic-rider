﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;

public class AdManager : MonoBehaviour {

	public static AdManager Instance { get; set; }
    private BannerView bannerView;
    private InterstitialAd interstitial;
    //float timer = 10f;


    void Start () {
        Instance = this;
        DontDestroyOnLoad(gameObject);

        string appId = "ca-app-pub-7194635251531037~5054066152";

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);
        SaveLoadSystem.StartSetup();

        ////////////////////////////
        SceneManager.LoadScene("Menu");
        ////////////////////////////
    }

    void Update()
    {

        if(Input.GetKey(KeyCode.Escape))
        {
            Scene name = SceneManager.GetActiveScene();
            if (name.name == "Game")
            {
                return;
            }
            else if (name.name == "End" || name.name == "Shop")
            {
                SceneManager.LoadScene("Menu");
            }
            else if (name.name == "Menu")
            {
                Application.Quit();
            }
        }
        /*if (timer <= 0f)
        {
            WorldVars.requester = true;
        }
        timer -= Time.deltaTime;*/
        // Bug disapeard when WorldVars.requester was moved to PlaterControlls.cs


        if (WorldVars.requester == true)
        {
            RequestBanner();
            RequestInterstitial();
            WorldVars.requester = false;
        }
        if(WorldVars.adFullscreen == true && WorldVars.adFullscreenOver == false)
        {
            
            ShowInterstitial();

            ////////////////////////////
            WorldVars.adFullscreenOver = true;
            WorldVars.resumeGame = true;
            ////////////////////////////

            RequestBanner();

        }
        

    }
    /*void OnAdClosed()
    {
        WorldVars.resumeGame = true;
    }
    */
    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("game")
            .SetGender(Gender.Male)
            .SetBirthday(new DateTime(1985, 1, 1))
            .TagForChildDirectedTreatment(false)
            .AddExtra("color_bg", "9B30FF")
            .Build();
    }

    public void DestroyBanner()
    {
        if (this.bannerView != null)
        {
            this.bannerView.Destroy();
        }
    }

    public void DestroyInterstitial()
    {
        if (this.interstitial != null)
        {
            this.interstitial.Destroy();
        }
    }

    public void RequestBanner()
    {
        
        string adUnitId = "ca-app-pub-7194635251531037/2372062497";


        // Clean up banner ad before creating a new one.
        if (this.bannerView != null)
        {
            this.bannerView.Destroy();
        }

        // Create a 320x50 banner at the top of the screen.
        this.bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);

        // Register for ad events.
        this.bannerView.OnAdLoaded += this.HandleAdLoaded;
        this.bannerView.OnAdFailedToLoad += this.HandleAdFailedToLoad;
        this.bannerView.OnAdOpening += this.HandleAdOpened;
        this.bannerView.OnAdClosed += this.HandleAdClosed;
        this.bannerView.OnAdLeavingApplication += this.HandleAdLeftApplication;

        // Load a banner ad.
        this.bannerView.LoadAd(this.CreateAdRequest());
    }

    public void RequestInterstitial()
    {

        string adUnitId = "ca-app-pub-7194635251531037/7568548859";


        // Clean up interstitial ad before creating a new one.
        if (this.interstitial != null)
        {
            this.interstitial.Destroy();
        }

        // Create an interstitial.
        this.interstitial = new InterstitialAd(adUnitId);

        // Register for ad events.
        this.interstitial.OnAdLoaded += this.HandleInterstitialLoaded;
        this.interstitial.OnAdFailedToLoad += this.HandleInterstitialFailedToLoad;
        this.interstitial.OnAdOpening += this.HandleInterstitialOpened;
        this.interstitial.OnAdClosed += this.HandleInterstitialClosed;
        this.interstitial.OnAdLeavingApplication += this.HandleInterstitialLeftApplication;

        // Load an interstitial ad.
        this.interstitial.LoadAd(this.CreateAdRequest());
    }

    public void ShowInterstitial()
    {
        if (this.interstitial.IsLoaded())
        {
            this.interstitial.Show();
        }
        else
        {
            MonoBehaviour.print("Interstitial is not ready yet");
        }
    }

    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeftApplication event received");
    }

    #endregion

    #region Interstitial callback handlers

    public void HandleInterstitialLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLoaded event received");
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleInterstitialFailedToLoad event received with message: " + args.Message);
    }

    public void HandleInterstitialOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialOpened event received");
    }

    public void HandleInterstitialClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialClosed event received");
    }

    public void HandleInterstitialLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleInterstitialLeftApplication event received");
    }

    #endregion
}
