﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameScore : MonoBehaviour
{
    public Text text;
    // Use this for initialization
    void Start()
    {
        text.text = WorldVars.score.ToString();
        SaveLoadSystem.SaveHighscore(WorldVars.score);
        WorldVars.money = WorldVars.money + WorldVars.score;
        SaveLoadSystem.SaveMoney();
    }
    void Update()
    {
        
    }
}

