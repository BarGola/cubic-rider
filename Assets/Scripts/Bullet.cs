﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float speed = 20f;
    Vector3 dir = Vector3.back;
    public GameObject fireEffect;

    public AudioClip collSound;
    public AudioSource collSource;

    // Use this for initialization
    void Start () {
        collSource.clip = collSound;
        collSource.volume = Random.Range(0.5f, 0.7f);
        collSource.pitch = Random.Range(0.6f, 1.0f);
        collSource.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if(transform.position.z >= 300 || transform.position.z <= -300)
        {
            Destroy(gameObject);
        }
        if (WorldVars.alive == true)
        {
            dir = Vector3.forward;
        }
        else if (WorldVars.alive == false)
        {
            dir = Vector3.back;
        }
        
        float distanceThisFrame = speed * Time.deltaTime;


        transform.Translate(dir.normalized * distanceThisFrame, Space.World);


    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Obs")
        {
            //collSource.Play();
            Instantiate(fireEffect, transform.position, transform.rotation);
            ObstacleMove obsHp = col.gameObject.GetComponent<ObstacleMove>();
            obsHp.hp -= Stats.damage;
            Destroy(gameObject);
        }
    }
}
