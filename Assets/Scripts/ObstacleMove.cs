﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMove : MonoBehaviour {
    Vector3 dir;
    bool hit = false;
    float timer = 0f;
    public int hp;
    public int scoreContainer;
    public GameObject obstacleSpawner;
    int[] lvlBorders = new int[10] {20, 50, 70, 120, 200, 300, 550, 750, 1200, 2000};
    //int[] lvlBorders = new int[10] { 5, 10 ,15, 150, 250, 380, 550, 750, 1200, 2000};
    int[] lvlHp = new int[10] { 3, 5, 8, 15, 22, 30, 40, 60, 90, 130};
    int[] lvlScore = new int[10] { 10, 25, 50, 75, 100, 150, 200, 275, 350, 500};
    public Color[] color = new Color[10];
    public Color hitColor;
    private Renderer rend;
    public GameObject explosionEffect;

    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        if (WorldVars.alive == true)
        {
            dir = Vector3.back;

            int obsCount = WorldVars.obsNumber;
            //int obsCount = ObstacleSpawner.obsSpawned.Capacity;

            for (int i = 0; i < lvlBorders.Length; i++)
            {
                if (i >= lvlBorders.Length - 1)
                {
                    hp = lvlHp[lvlBorders.Length - 1];
                    rend.material.color = color[lvlBorders.Length - 1];
                    scoreContainer = lvlScore[lvlBorders.Length - 1];
                    return;
                }
                else if (obsCount <= lvlBorders[i])
                {

                    hp = lvlHp[i];
                    rend.material.color = color[i];
                    scoreContainer = lvlScore[i];
                    return;
                }
            }
        }
        else if (WorldVars.alive == false)
        {
            dir = Vector3.forward;

            int obsCount = WorldVars.obsNumber;

            for (int i = 0; i < lvlBorders.Length; i++)
            {
                if (i >= lvlBorders.Length - 1)
                {
                    hp = lvlHp[lvlBorders.Length - 1];
                    rend.material.color = color[lvlBorders.Length - 1];
                    scoreContainer = lvlScore[lvlBorders.Length - 1];
                    return;
                }
                else if (obsCount <= lvlBorders[i])
                {

                    hp = lvlHp[i];
                    rend.material.color = color[i];
                    scoreContainer = lvlScore[i];
                    return;
                }
            }
        }

        



    }
	
	// Update is called once per frame
	void Update () {
        if (WorldVars.obsDest == true)
        {
            Destroy(gameObject);
        }

        if (hit == false)
        {
            //transform.position += dir * WorldVars.worldSpeed * Time.deltaTime;

            float distanceThisFrame = WorldVars.worldSpeed * Time.deltaTime;
            transform.Translate(dir.normalized * distanceThisFrame, Space.World);

            if (WorldVars.alive == true)
            {
                if (transform.position.z <= -50 || transform.position.z >= 300)
                {
                    Destroy(gameObject);
                }
            }
            else if (WorldVars.alive == false)
            {
                if (transform.position.z >= 50 || transform.position.z <= -300)
                {
                    Destroy(gameObject);
                }
            }
            if(hp <= 0)
            {

                WorldVars.score += scoreContainer;
                DestroyObs();
                
                
            }
        }
        if (hit == true)
        {
            if (timer <= 0)
            {
                DestroyObs();
            }
            
            timer -= Time.deltaTime;
        }



    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player" || col.gameObject.tag == "Obs")
        {
            rend = GetComponent<Renderer>();
            if (WorldVars.alive == true)
            {
                timer = 0.001f;
            }
            else if (WorldVars.alive == false)
            {
                timer = 0.001f;
            }
            rend.material.color = hitColor;
            hit = true;

        }
    }
    void DestroyObs()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
