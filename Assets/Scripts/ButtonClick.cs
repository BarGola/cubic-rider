﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class ButtonClick : MonoBehaviour {

    public AudioClip buttonSound;
    public AudioSource source;



	// Use this for initialization
	void Start () {
        source.clip = buttonSound;
	}
	
	void OnClick()
    {
        source.Play();
    }
}
