﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    public Text text;
    public Color color;
    public Color colorChange;
    bool colorCh = false;

	// Use this for initialization
	void Start () {
        color = text.color;

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        text.text = WorldVars.score.ToString();

        if (WorldVars.alive == false && colorCh == false)
        {
            text.color = colorChange;
            colorCh = true;
        }
	}
}
