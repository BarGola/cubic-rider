﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldVars : MonoBehaviour {

    public static float worldSpeed = 50f;
    public static float worldDirection = 1f;
    public static bool alive = true;
    public static bool gameOver = false;
    public static bool death = false;
    public static float targetAlpha = 1f;
    public static int obsNumber = 0;
    public static bool obsDest = false;

    public static bool adFullscreen = false;
    public static bool adFullscreenOver = false;

    public static bool resumeGame = false;

    public static int score = 0;
    public static int money;

    public static bool requester = false;




    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void ResetVars()
    {
        worldSpeed = 50f;
        worldDirection = 1f;
        alive = true;
        gameOver = false;
        death = false;
        targetAlpha = 1f;
        obsNumber = 0;
        obsDest = false;
        adFullscreen = false;
        adFullscreenOver = false;
        resumeGame = false;
        score = 0;
        requester = false;
    }
}
