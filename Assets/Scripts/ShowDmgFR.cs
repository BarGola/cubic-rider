﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowDmgFR : MonoBehaviour {

    public Text text;
    public string toShow;
    public UpgradeButton a;

    private void Start()
    {
        a = GetComponent<UpgradeButton>();
    }
    private void Update()
    {
        if (toShow == "DMG")
        {
            ShowDMG();
        }
        else if (toShow == "FR")
        {
            ShowFR();
        }
        else if (toShow == "Money")
        {
            ShowMoney();
        }
        else if (toShow == "FRCost")
        {
            ShowFRCost();
        }
        else if (toShow == "DMGCost")
        {
            ShowDMGCost();
        }
    }

    public void ShowMoney()
    {
        text.text = WorldVars.money.ToString();
    }

    public void ShowDMG()
    {
        text.text = Stats.damage.ToString();
    }

    public void ShowFR()
    {
        text.text = Stats.fireRate.ToString();
    }
    public void ShowFRCost()
    {
        text.text = a.ShowCostFR();
    }
    public void ShowDMGCost()
    {
        text.text = a.ShowCostDMG();
    }
}
