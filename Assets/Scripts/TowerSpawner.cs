﻿using UnityEngine;

public class TowerSpawner : MonoBehaviour {

    float timerL = 1f;
    float timerR = 1f;


    Vector3 posL;
    Vector3 posR;

    Quaternion rot;

    public GameObject[] leftTowers = new GameObject[3];
    public GameObject[] rightTowers = new GameObject[3];

    public GameObject objToRotate;

    


    // Use this for initialization
    void Start () {
        rot = objToRotate.transform.rotation;
        Destroy(objToRotate);
        posR = new Vector3(25, 0, 500);
        posL = new Vector3(-25, 0, 500);
    }
	
	// Update is called once per frame
	void Update () {
        if(WorldVars.alive == false)
        {
            posL.z = posL.z * -1;
            posR.z = posR.z * -1;
        }
		if (timerL <= 0)
        {
            randomTowerSpawner(ref leftTowers, posL);
            timerL = Random.Range(1, 3);
        }

        if (timerR <= 0)
        {
            randomTowerSpawner(ref rightTowers, posR);
            timerR = Random.Range(1, 3);
        }
        timerL -= Time.deltaTime;
        timerR -= Time.deltaTime;

    }

    void randomTowerSpawner(ref GameObject[] x, Vector3 pos)
    {

        int temp = Random.Range(0, x.Length);
        Instantiate(x[temp], pos + new Vector3(Random.Range(-7, 7), 0, 0), rot);

    }
}
