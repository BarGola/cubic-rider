﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingMove : MonoBehaviour {
    Vector3 dir;

	// Update is called once per frame
	void Update () {
        if (WorldVars.alive == true)
        {
            dir = Vector3.back;
        }
        else if (WorldVars.alive == false)
        {
            dir = Vector3.forward;
        }

        if (transform.position.z <= -550 || transform.position.z >= 550)
        {
            Destroy(gameObject);
        }

        transform.position += dir * WorldVars.worldSpeed * Time.deltaTime;
        
    }
}
