﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObstacleSpawner : MonoBehaviour {

    public static List<GameObject> obsSpawned = new List<GameObject>();
    public GameObject[] obsCollection = new GameObject[3];
    float timer = 1f;
    public static int obsDeadCount = 0;

    public float timeBetween = 1.1f;
    bool deadChanges = false;

    public float endGameTimer = 10f;
    bool endGameBool = false;
    float lastCountdown = 5f;


    Vector3 posToSpawnAlive = new Vector3(0, 6, 250);
    Vector3 posToSpawnDead = new Vector3(0, 6, -250);



    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

        if(WorldVars.death == false)
        {
            

            if (WorldVars.alive == true)
            {
                if (timer <= 0)
                {

                    CreateObs(ref timer, ref obsSpawned);

                }
            }
            else if (WorldVars.alive == false)
            {
                if (deadChanges == false)
                {
                    deadChanges = true;
                }
                if (timer <= 0)
                {

                    CreateObs(ref timer, ref obsSpawned);

                }
            }

        }
        timer -= Time.deltaTime;
    }

    public void CreateObs(ref float obsTimer, ref List<GameObject> obsList)
    {
        if (WorldVars.alive == true)
        {
            int tmp = Random.Range(0, obsCollection.Length);

            Instantiate(obsCollection[tmp], posToSpawnAlive, transform.rotation);
            obsTimer = timeBetween;
            obsList.Add(obsCollection[tmp]);
            WorldVars.obsNumber += 1;
            WorldVars.score += 5;

        }
        else if (WorldVars.alive == false)
        {
            if (WorldVars.obsNumber > 0)
            {
                if (endGameBool == false)
                {
                    WorldVars.obsNumber -= 1;
                    Instantiate(obsSpawned[WorldVars.obsNumber], posToSpawnDead, transform.rotation);
                    obsTimer = timeBetween;
                    
                }
                
                
            }
            else if (WorldVars.obsNumber <= 0)
            {
                if (endGameBool == false)
                {
                    endGameBool = true;
                }
                
            }

            if(endGameBool == true)
            {
                lastCountdown -= Time.deltaTime;
            }


            if (endGameBool == true && lastCountdown <= 0f)
            {
                WorldVars.targetAlpha = 1f;
                if (endGameTimer <= 0f)
                {

                    SceneManager.LoadScene("End");
                }
                endGameTimer -= Time.deltaTime;
            }
        }

    }


}
