﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControlls : MonoBehaviour {
    public float speed = 1f;
    bool firstDeath = true;
    bool startRotation = false;
    bool startGame = true;
    float deathTimer = 4f;
    Vector3 dir = Vector3.back;
    public Transform partToRotate;
    public float rotationSpeed = 5f;
    public GameObject bulletPrefab;
    public Transform firePoint;
    public GameObject fireEffect;
    private float fireCountdown = 2.5f;
    float lastDeathTimer = 2f;
    public Rigidbody rb;
    bool doThisOnce = false;
    int leftMove = 0;
    int rightMove = 0;



    public AudioClip collSound;
    public AudioSource collSource;

    //public AudioScript audioScript;





    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;


        collSource.clip = collSound;
        

    }

    // Update is called once per frame
    void Update () {
        if (WorldVars.death == false)
        {

            if (rightMove == 2)
            {
               

                if (WorldVars.alive == true)
                {
                    rb.velocity = Vector3.right * speed / 50;

                }
                else if (WorldVars.alive == false)
                {
                    rb.velocity = Vector3.left * speed / 50;

                }



            }
            else if (leftMove == 2)
            {

                if (WorldVars.alive == true)
                {
                    rb.velocity = Vector3.left * speed / 50;

                }
                else if (WorldVars.alive == false)
                {
                    rb.velocity = Vector3.right * speed / 50;

                }


            }
            else if (leftMove < 2 && rightMove <2)
            {
                rb.velocity = Vector3.zero;
            }




            //Arrow controls

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                LeftMove();
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                RightMove();
            }
            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                StopMoveLeft();
            }
            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                StopMoveRight();
            }







        }

        if (startGame == true)
        {
            
            WorldVars.targetAlpha = 0f;
            startGame = false;
        }

        if (WorldVars.death == false)
        {
            if (WorldVars.alive == false)
            {
                if(fireCountdown <= 0f)
                {
                    Shoot();
                    fireCountdown = 50f / Stats.fireRate;
                }
                fireCountdown -= Time.deltaTime;




            }




            //Death when below y=1
            if (transform.position.x >= 5.2f || transform.position.x <= -5.2f)
            {
                rb.useGravity = true;
                StopMoveLeft();
                StopMoveRight();
                if(WorldVars.alive == true)
                {
                    WorldVars.death = true;
                }
                else if (WorldVars.alive == false)
                {
                    rb.useGravity = true;
                    WorldVars.targetAlpha = 1f;
                    if(lastDeathTimer <= 0f)
                    {
                        SceneManager.LoadScene("End");
                    }
                    lastDeathTimer -= Time.deltaTime;
                }
            }
            /*
            //Touch controlls
            if(WorldVars.alive == true)
            {
                dirX = CrossPlatformInputManager.GetAxis("Horizontal");
                if(dirX < 0)
                {
                    dirX = -1;
                }
                else if(dirX >0)
                {
                    dirX = 1;
                }
                transform.position += new Vector3(dirX, 0, 0) * speed * Time.deltaTime;
            }
            else if (WorldVars.alive == false)
            {
                dirX = CrossPlatformInputManager.GetAxis("Horizontal");
                if (dirX < 0)
                {
                    dirX = -1;
                }
                else if (dirX > 0)
                {
                    dirX = 1;
                }
                transform.position -= new Vector3(dirX, 0, 0) * speed * Time.deltaTime;
            }



            //Arrow controls
            if (WorldVars.alive == true)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.position += Vector3.left * speed * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    transform.position += Vector3.right * speed * Time.deltaTime;
                }
            }
            else if (WorldVars.alive == false)
            {
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.position -= Vector3.left * speed * Time.deltaTime;
                }
                else if (Input.GetKey(KeyCode.RightArrow))
                {
                    transform.position -= Vector3.right * speed * Time.deltaTime;
                }
            }
            */

                // Locking z to 0
                if (transform.position.z != 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, 0);
            }
            

        }

        if(WorldVars.death == true && firstDeath == true && startRotation == false)
        {
            rightMove = 0;
            leftMove = 0;
            rb.velocity = Vector3.zero;
            WorldVars.targetAlpha = 1f;
            if (deathTimer <= 0)
            {

                startRotation = true;
                
            }
            deathTimer -= Time.deltaTime;
        }

        /*
        Vector3 dir = target.position - transform.position;
        Quaternion lookDir = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookDir, Time.deltaTime * rotationSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        */
        
        if (WorldVars.death == true && firstDeath == true && startRotation == true)
        {
            
            
            WorldVars.adFullscreen = true;
            WorldVars.obsDest = true;
            Quaternion lookDir = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookDir, Time.deltaTime * rotationSpeed).eulerAngles;
            partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);
            if(doThisOnce == false)
            {
                transform.position = new Vector3(0, 6, 0);
                transform.rotation = lookDir;
                doThisOnce = true;
            }
            
            if (partToRotate.rotation == lookDir && WorldVars.resumeGame == true)
            {
                rb.useGravity = false;
                WorldVars.obsDest = false;
                
                WorldVars.alive = false;
                WorldVars.death = false;
                firstDeath = false;
                WorldVars.targetAlpha = 0f;
                

            }
            
        }

        

    }
    /*void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Obs" && WorldVars.alive == true)
        {

            if (WorldVars.death == false)
            {
                WorldVars.death = true;
            }

        }

    }*/
    

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Obs" && WorldVars.alive == true)
        {
            collSource.Play();

            if (WorldVars.death == false)
            {
                WorldVars.death = true;
            }

        }

    }

    void Shoot()
    {
        //shootSource.volume = Random.Range(0.7f, 1.0f);
        //shootSource.pitch = Random.Range(0.9f, 1.0f);
        //shootSource.Play();
        Instantiate(fireEffect, firePoint.position, firePoint.rotation);
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        
    }

    public void LeftMove()
    {
        if (rightMove == 2)
        {
            leftMove = 1;
        }
        else if (rightMove == 0)
        {
            leftMove = 2;
        }





    }

    public void RightMove()
    {
        if (leftMove == 2)
        {
            rightMove = 1;
        }
        else if (leftMove == 0)
        {
            rightMove = 2;
        }





    }

    public void StopMoveLeft()
    {
        if (rightMove == 0)
        {
            leftMove = 0;
        }
        else if (rightMove == 1)
        {
            leftMove = 0;
            rightMove = 2;
        }




    }


    public void StopMoveRight()
    {
        if (leftMove == 0)
        {
            rightMove = 0;
        }
        else if (leftMove == 1)
        {
            rightMove = 0;
            leftMove = 2;
        }




    }


}
