﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;




public class BackgroundMusic : MonoBehaviour {

    static BackgroundMusic instance = null;
    
	// Use this for initialization
	void Start () {

        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
