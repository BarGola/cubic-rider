﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsCreateAndDestroy : MonoBehaviour {
    public float timer = 5f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(timer <=0)
        {
            Destroy(gameObject);
        }

        timer -= Time.deltaTime;
    }
}
