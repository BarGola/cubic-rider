﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TouchControls : MonoBehaviour {

    float dirX;
    //Rigidbody rb;


	// Use this for initialization
	void Start () {
        //rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        dirX = CrossPlatformInputManager.GetAxis("Horizontal");
        //rb.velocity = new Vector3(dirX * 5, 0, 0);
        transform.position += new Vector3(dirX * 100, 0, 0) * Time.deltaTime;
    }
}
