﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayButton : MonoBehaviour {

    public string sceneName;

	public void PlayScript()
    {
        if(sceneName == "Game")
        {
            WorldVars.ResetVars();
            Debug.Log("WorldVars reset!");
            WorldVars.requester = true;
        }
        SceneManager.LoadScene(sceneName);
    }
}
