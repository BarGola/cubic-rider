﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsCreate : MonoBehaviour {
    public GameObject obs;
	// Use this for initialization
	void Start () {
        Instantiate(obs, transform.position, transform.rotation);
        Destroy(gameObject);
	}

}
