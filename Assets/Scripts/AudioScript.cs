﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript : MonoBehaviour {

    public AudioClip sound;

    public AudioSource source;

	// Use this for initialization
	void Start () {
        source.clip = sound;
	}
	
	public void PlayClipcC()
    {
        source.Play();
    }
}
