﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeButton : MonoBehaviour {

    int[] costTableFr = new int[9] { 100, 700, 4000, 20000, 70000, 300000, 500000, 800000, 1000000 };
    int[] costTableDmg = new int[9] { 100, 700, 4000, 20000, 70000, 300000, 500000, 800000, 1000000 };
    float[] frLevels = new float[9] { 100, 150, 200, 250, 300, 350, 400, 450, 500 };
    int[] dmgLevels = new int[9] { 1, 2, 3, 4, 5, 6, 7, 8, 10 };



    public void UpFireRate()
    {
        UpgradeFireRate(ref Stats.fireRate, ref WorldVars.money);
    }

    public void UpDamage()
    {
        UpgradeDamage(ref Stats.damage, ref WorldVars.money);
    }

    public string ShowCostDMG()
    {
        for (int i = 0; i < dmgLevels.Length; i++)
        {
            if (i < dmgLevels.Length - 1)
            {
                if (Stats.damage == dmgLevels[i])
                {
                    return costTableDmg[i].ToString();

                }
            }

        }
        return "MAX";
    }

    public string ShowCostFR()
    {
        for (int i = 0; i < frLevels.Length; i++)
        {
            if (i < frLevels.Length - 1)
            {
                if (Stats.fireRate == frLevels[i])
                {
                    return costTableFr[i].ToString();

                }
            }

        }
        return "MAX";
    }



    public void UpgradeFireRate(ref float a, ref int money)
    {
        
        for(int i = 0;i < frLevels.Length; i++ )
        {
            if (i < frLevels.Length-1)
            {
                if (a == frLevels[i])
                {
                    if (money >= costTableFr[i])
                    {
                        money -= costTableFr[i];
                        a = frLevels[i + 1];
                        SaveLoadSystem.SaveMoney();
                        SaveLoadSystem.SaveStats();

                        return;
                    }
                    
                }
            }
            
        }
    }


    public void UpgradeDamage(ref int a, ref int money)
    {

        for (int i = 0; i < dmgLevels.Length; i++)
        {
            if (i < dmgLevels.Length - 1)
            {
                if (a == dmgLevels[i])
                {
                    if (money >= costTableDmg[i])
                    {
                        money -= costTableDmg[i];
                        a = dmgLevels[i + 1];
                        SaveLoadSystem.SaveMoney();
                        SaveLoadSystem.SaveStats();

                        return;
                    }

                }
            }

        }
    }


}
